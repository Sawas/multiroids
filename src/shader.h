#ifndef MULTIROIDS_SHADER_H
#define MULTIROIDS_SHADER_H

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path);

#endif //MULTIROIDS_SHADER_H
