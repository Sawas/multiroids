#ifndef MULTIROIDS_CONTROLS_H
#define MULTIROIDS_CONTROLS_H

#include <glm.hpp>

void computeMatricesFromInputs();
glm::mat4 getViewMatrix();
glm::mat4 getProjectionMatrix();

#endif //MULTIROIDS_CONTROLS_H
